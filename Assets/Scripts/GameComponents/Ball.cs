﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    #region Variables and Properties
    #region Components
    private Transform m_Transform;
	public Transform Transform { get { if (!m_Transform) m_Transform = transform; return m_Transform; } }
	private Rigidbody m_Rigidbody;
	public Rigidbody Rigidbody { get { if (!m_Rigidbody) m_Rigidbody = GetComponent<Rigidbody>(); return m_Rigidbody; } }
	private BallView m_BallView;
	public BallView BallView { get { if (!m_BallView) m_BallView = GetComponentInChildren<BallView>(); return m_BallView; } }
    #endregion

    public bool IsAIBall { get; private set; }
    public bool ScoredThisShot { get; private set; }
    public int Score { get; private set; }
    private bool isPerfectShot;
    private bool backboardBonus;
    private bool isLuckyShot;
    private bool isFireBall;
    #endregion

    #region Monobehaviours
    private void OnCollisionEnter(Collision collision)
    {
        if (ScoredThisShot)
            return;

        if(collision.gameObject.CompareTag(TagsAndLayers.BackboardTag) ||
           collision.gameObject.CompareTag(TagsAndLayers.BasketRingTag))
        {
            isPerfectShot = false;

            if(collision.gameObject.CompareTag(TagsAndLayers.BackboardTag))
            {
                if (Basket.singleInstance.BackboardBonusEnabled)
                    backboardBonus = true;
            }

            //Sound
            AudioManager.singleInstance.Play(PoolTags.BallBounceBackboard);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag(TagsAndLayers.BasketScoreTriggerTag))
        {
            if (!ScoredThisShot)
            {
                ScoredThisShot = true;
                SetScore();

                float fireballProgressGained = isPerfectShot || backboardBonus ? ShootingRaceHandler.fireBallData.PerfectShotFireBallGain : ShootingRaceHandler.fireBallData.StandardShotFireBallGain;

                GlobalEvents.BallScored?.Invoke(IsAIBall, Score, fireballProgressGained);
                if (backboardBonus)
                    GlobalEvents.BackboardBonusScored?.Invoke();
                if (isPerfectShot)
                    GlobalEvents.PerfectShot?.Invoke(IsAIBall);

                NotificationType notification = NotificationType.StandardScore;
                if (!IsAIBall) //AI only sends standard notifications
                {
                    if (backboardBonus)
                        notification = NotificationType.BackboardScore;
                    else if (isPerfectShot)
                        notification = NotificationType.PerfectScore;
                }

                GlobalEvents.Notify?.Invoke(IsAIBall, notification, Score);

                //Sound
                if(isLuckyShot)
                    AudioManager.singleInstance.Play(PoolTags.LuckyBallScored);
                else
                    AudioManager.singleInstance.Play(PoolTags.BallScored);
            }
        }
        if(other.gameObject.CompareTag(TagsAndLayers.GroundTag))
        {
            GlobalEvents.ShotCompleted?.Invoke(IsAIBall, ScoredThisShot);
            PoolManager.singleInstance.Push(PoolTags.Ball, this.gameObject);

            //Sound
            AudioManager.singleInstance.Play(PoolTags.BallBounceGround);
        }
    }
    #endregion

    #region Methods
    public void Init(Vector3 shotPosition, bool isLuckyShot = false, bool isFireBall = false, bool isAIBall = false) 
    {
        ScoredThisShot = false;
        isPerfectShot = true;
        backboardBonus = false;
        this.isLuckyShot = isLuckyShot;
        this.isFireBall = isFireBall;
        this.IsAIBall = isAIBall;
        Score = 0;

        Rigidbody.useGravity = false;
        Rigidbody.velocity = Vector3.zero;
        Transform.position = shotPosition;

        BallView.Init(isLuckyShot, isFireBall);
    }
    public void Shoot(float force, Vector3 direction)
    {
        Rigidbody.useGravity = true;
        Rigidbody.AddForce(force * direction, ForceMode.Impulse);

        BallView.OnShoot(direction);
    }
    private void SetScore()
    {
        //Base score
        if (backboardBonus)
            Score = Basket.singleInstance.BackboardData.scoreBonus;
        else if (isPerfectShot)
            Score = ShootingRaceHandler.Scores.PerfectShotScore;
        else
            Score = ShootingRaceHandler.Scores.StandardShotScore;

        //Multipliers
        if (isLuckyShot)
            Score *= ShootingRaceHandler.Scores.LuckyBallMultiplier;
        if (isFireBall)
            Score *= ShootingRaceHandler.fireBallData.FireBallMultiplier;
    }
    #endregion
}
