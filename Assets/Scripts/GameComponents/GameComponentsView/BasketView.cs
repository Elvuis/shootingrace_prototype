﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasketView : MonoBehaviour
{
	#region Variables and Properties
	[SerializeField] protected TMPro.TextMeshProUGUI scoreText;
	[SerializeField] protected Image BackboardHighlight;
	[SerializeField] protected Image scoreCircle;
	[Space(10)]
	[SerializeField] protected ParticleSystem perfectShotParticleSystem;
	#endregion

	#region Monobehaviours

	#endregion

	#region Methods
	public void EnableBackboardBonusView(bool enable, BackboardBonusData bonusData = null)
	{
		if (bonusData != null)
		{
			this.scoreText.text = bonusData.scoreBonus.ToString();
			this.scoreText.color = bonusData.colorBonus;
			scoreCircle.color = bonusData.colorBonus;
			BackboardHighlight.color = bonusData.colorBonus;
		}

		this.scoreText.gameObject.SetActive(enable);
		scoreCircle.gameObject.SetActive(enable);
		BackboardHighlight.gameObject.SetActive(enable);
	}

	public void PlayParticleSystem()
	{
		if(!perfectShotParticleSystem.isPlaying)
			perfectShotParticleSystem.Play();
	}
	#endregion
}
