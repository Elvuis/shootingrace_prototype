﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallView : MonoBehaviour
{
	#region Variables and Properties
	[SerializeField] private Material baseMaterial;
	[SerializeField] private Material luckyBallMaterial;
	[Space(10)]
	[SerializeField] private ParticleSystem fireBallParticleSystem;
	[SerializeField] private ParticleSystem luckyBallParticleSystem;

	private MeshRenderer m_MeshRenderer;
	private MeshRenderer MeshRenderer { get { if (!m_MeshRenderer) m_MeshRenderer = GetComponent<MeshRenderer>(); return m_MeshRenderer; } }
	private Transform m_Transform;
	private Transform Transform { get { if (!m_Transform) m_Transform = transform; return m_Transform; } }

	private bool rotate;
	private Vector3 rotationDirection;
	[SerializeField] private float rotationSpeed;
	#endregion

	#region Monobehaviours
	private void OnDisable()
	{
		MeshRenderer.material = baseMaterial;
		if (luckyBallParticleSystem.isPlaying)
			luckyBallParticleSystem.Stop();
		if (fireBallParticleSystem.isPlaying)
			fireBallParticleSystem.Stop();

		rotate = false;
	}
	private void Update()
	{
		if (rotate)
			Transform.Rotate(rotationDirection * rotationSpeed * Time.deltaTime);
	}
	#endregion

	#region Methods
	public void Init(bool isLuckyBall, bool isFireBall)
	{
		if (isLuckyBall)
		{
			MeshRenderer.material = luckyBallMaterial;
			luckyBallParticleSystem.Play();
		}

		if (isFireBall)
		{
			fireBallParticleSystem.Play();
		}
	}

	public void OnShoot(Vector3 ballDirection)
	{
		rotate = true;
		rotationDirection = new Vector3(-ballDirection.y, ballDirection.z);
		//rotationDirection = ballDirection;
	}
	#endregion
}
