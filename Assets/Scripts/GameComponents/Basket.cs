﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Basket : Singleton<Basket>
{
	#region Variables and Properties
	private Transform m_Transform;
	public Transform Transform { get { if (!m_Transform) m_Transform = transform; return m_Transform; } }	
	private BasketView m_BasketView;
	private BasketView BasketView { get { if (!m_BasketView) m_BasketView = GetComponentInChildren<BasketView>(); return m_BasketView; } }

	public bool BackboardBonusEnabled { get; private set; }
	public BackboardBonusData BackboardData { get; private set; }
	#endregion

	#region Monobehaviours
	private void OnEnable()
	{
		GlobalEvents.BackboardBonusScored += ResetBackboardBonus;
		GlobalEvents.EnableBackboardBonus += OnEnableBackboardBonus;
		GlobalEvents.GameStarted += OnGameStarted;
		GlobalEvents.PerfectShot += OnPerfectShot;
	}
	private void OnDisable()
	{
		GlobalEvents.BackboardBonusScored -= ResetBackboardBonus;
		GlobalEvents.EnableBackboardBonus -= OnEnableBackboardBonus;
		GlobalEvents.PerfectShot -= OnPerfectShot;
	}
	#endregion

	#region Methods
	public Vector3 GetDirectionFromTargetPosition(Vector3 targetPoint)
	{
		return (transform.position - targetPoint).normalized;
	}
	
	private void OnGameStarted(bool vsAI)
	{
		ResetBackboardBonus();
	}

	private void ResetBackboardBonus()
	{
		BackboardBonusEnabled = false;
		BackboardData = null;
		BasketView.EnableBackboardBonusView(false);
	}

	private void OnEnableBackboardBonus()
	{
		BackboardBonusEnabled = true;
		BackboardData = ShootingRaceHandler.Scores.GetBackboardBonusScore();
		BasketView.EnableBackboardBonusView(true, BackboardData);
	}

	private void OnPerfectShot(bool isAIBall)
	{
		BasketView.PlayParticleSystem();
	}
	#endregion
}
