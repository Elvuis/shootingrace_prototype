﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UI_RectPositionTweener : MonoBehaviour
{
	#region Variables and Properties
	private RectTransform m_RectTransform;
	private RectTransform RectTransform { get { if (!m_RectTransform) m_RectTransform = GetComponent<RectTransform>(); return m_RectTransform; } }

	#region Structs
	[System.Serializable]
	public struct InterpolationData
	{
		public Vector2 startingAnchoredPosition;
		public Vector2 targetAnchoredPosition;
		public float transitionDuration;
		public AnimationCurve transitionCurve;
	}
	#endregion

	[SerializeField] private bool playOnEnable;
	[SerializeField] private InterpolationData[] transitions;
	[Space(10)]
	[SerializeField] private UnityEvent onTweenEnded;

	public RectTweenerStates PlayingState { get; private set; } = RectTweenerStates.Disabled;
	/// <summary>
	/// This bool is used in the PlayOpposite() method.
	/// </summary>
	private bool lastPlayWasReverse = false;
	private int currentTransitionIndex;
	private CustomTimer transitionTimer;
	private InterpolationData CurrentTransition => transitions[currentTransitionIndex];
	private float EvaluatedProgress
	{
		get 
		{
			float result = CurrentTransition.transitionCurve.Evaluate(transitionTimer.Progress);

			if (PlayingState == RectTweenerStates.ReversePlaying)
				result = 1 - result;

			return result;
		} 
	}

	private Vector2 objectStartingPosition;
	#endregion

	#region Monobehaviours
	private void Awake()
	{
		objectStartingPosition = RectTransform.anchoredPosition;
	}
	private void OnEnable()
	{
		if (playOnEnable)
			Play(false);
	}
	private void Update()
	{
		if(PlayingState != RectTweenerStates.Disabled)
			ProcessInterpolation();
	}
	#endregion

	#region Methods
	public void Play(bool reversePlay)
	{
		//SetStartingPosition
		RectTransform.anchoredPosition = !reversePlay ? objectStartingPosition : transitions[transitions.Length - 1].targetAnchoredPosition;

		currentTransitionIndex = reversePlay ? transitions.Length - 1 : 0;
		StartTransitionTimer();
		PlayingState = reversePlay ? RectTweenerStates.ReversePlaying : RectTweenerStates.Playing;
	}

	private void ProcessInterpolation()
	{
		transitionTimer.Clock();
		RectTransform.anchoredPosition = Vector2.Lerp(CurrentTransition.startingAnchoredPosition, CurrentTransition.targetAnchoredPosition, EvaluatedProgress);

		if (transitionTimer.Expired)
		{
			currentTransitionIndex = PlayingState == RectTweenerStates.Playing ? currentTransitionIndex + 1 : currentTransitionIndex - 1;

			if (currentTransitionIndex < 0 || currentTransitionIndex >= transitions.Length)
			{
				lastPlayWasReverse = PlayingState == RectTweenerStates.ReversePlaying;
				PlayingState = RectTweenerStates.Disabled;
				transitionTimer.Stop();
				onTweenEnded?.Invoke();
				return;
			}

			StartTransitionTimer();
		}
	}

	private void StartTransitionTimer()
	{
		transitionTimer = new CustomTimer(CurrentTransition.transitionDuration);
		transitionTimer.Start();
	}

	public void ResetPositionToStart()
	{
		RectTransform.anchoredPosition = objectStartingPosition;

	}

	/// <summary>
	/// Call this method to make the tweener play toward the opposite direction.
	/// Usefull for loops.
	/// </summary>
	public void PlayOpposite()
	{
		Play(lastPlayWasReverse ? false : true);
	}

	public void Stop()
	{
		transitionTimer.Stop();
	}
	#endregion
}
