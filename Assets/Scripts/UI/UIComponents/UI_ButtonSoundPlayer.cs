﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ButtonSoundPlayer : MonoBehaviour
{
	#region Variables and Properties

	#endregion

	#region Monobehaviours
	private void Start()
	{
		GetComponent<Button>()?.onClick.AddListener(() => AudioManager.singleInstance.Play(PoolTags.UIButton));
	}
	#endregion

	#region Methods

	#endregion
}
