﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_InputTrailRenderer : MonoBehaviour
{
	#region Variables and Properties
	private TrailRenderer m_TrailRenderer;
	private TrailRenderer TrailRenderer { get { if (!m_TrailRenderer) m_TrailRenderer = GetComponent<TrailRenderer>(); return m_TrailRenderer; } }
	private RectTransform m_RectTransform;
	private RectTransform RectTransform { get { if (!m_RectTransform) m_RectTransform = gameObject.GetComponent<RectTransform>(); return m_RectTransform; } }
	private Canvas m_Canvas;
	private Canvas Canvas { get { if (!m_Canvas) m_Canvas = GetComponentInParent<Canvas>(); return m_Canvas; } }
	#endregion

	#region Monobehaviours
	private void OnEnable()
	{
		GlobalEvents.DisableInputTrailRenderer += DisableInputTrailRenderer;
		GlobalEvents.InputMoved += OnInputMoved;
	}
	private void OnDisable()
	{
		GlobalEvents.DisableInputTrailRenderer -= DisableInputTrailRenderer;
		GlobalEvents.InputMoved -= OnInputMoved;
	}
	#endregion

	#region Methods
	private void DisableInputTrailRenderer()
	{
		TrailRenderer.enabled = false;
	}
	private void OnInputMoved(Vector3 viewportPos)
	{
		RectTransform.anchoredPosition = Camera.main.ViewportToScreenPoint(viewportPos) * Canvas.scaleFactor;
		if (!TrailRenderer.enabled)
		{
			TrailRenderer.Clear();
			TrailRenderer.enabled = true;
		}

	} 
	#endregion
}
