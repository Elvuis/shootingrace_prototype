﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_CountDown : MonoBehaviour
{
	#region Variables and Properties
	[SerializeField] private TMPro.TextMeshProUGUI countDownText;
	[SerializeField] private CustomTimer countDownTimer;

	private string newText = "";
	#endregion

	#region Monobehaviours
	private void Update()
	{
		if (countDownTimer != null && countDownTimer.IsActive)
		{
			countDownTimer.Clock();
			newText = Mathf.CeilToInt(countDownTimer.Duration - countDownTimer.CurrentTime).ToString();
			if(countDownText.text != newText)
			{
				countDownText.text = newText;
				//Sound
				if (!countDownTimer.Expired)
					AudioManager.singleInstance.Play(PoolTags.CountDownBeep);
			}
		}
	}
	#endregion

	#region Methods
	public void StartCountDown()
	{
		countDownText.text = countDownTimer.Duration.ToString();
		countDownTimer.Start();

		//Sound
		AudioManager.singleInstance.Play(PoolTags.CountDownBeep);
	}
	public void CountDownExpired()
	{
		countDownTimer.Stop();
		GlobalEvents.CountDownEnded?.Invoke();

		//Sound
		AudioManager.singleInstance.Play(PoolTags.CountDownEnd);
	}
	#endregion
}
