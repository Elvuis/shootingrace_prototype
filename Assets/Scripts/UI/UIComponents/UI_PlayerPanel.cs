﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI_PlayerPanel : MonoBehaviour
{
	#region Variables and Properties
	[SerializeField] protected Image gameTimerFill;
	[SerializeField] protected TextMeshProUGUI scoreText;
	[SerializeField] protected UI_RectPositionTweener onFirePanel;
	private bool firePanelEnabled;

	private float previousScore = 0;
	#endregion

	#region Monobehaviours
	#endregion

	#region Methods
	public void SetScoreText(int score)
	{
		previousScore += score;
		scoreText.text = previousScore.ToString();
	}

	public void SetGameTimerFillAmount(float progress)
	{
		gameTimerFill.fillAmount = 1 - progress;
	}

	public void EnableOnFirePanel(bool enable)
	{
		if(enable != firePanelEnabled && onFirePanel.PlayingState == RectTweenerStates.Disabled)
		{
			onFirePanel.Play(!enable);
			firePanelEnabled = enable;
		}
	}

	public void Init()
	{
		previousScore = 0;
		SetScoreText(0);
		SetGameTimerFillAmount(0);
		firePanelEnabled = false;
		onFirePanel.ResetPositionToStart();
	}
	#endregion
}
