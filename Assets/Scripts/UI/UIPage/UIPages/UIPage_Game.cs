﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIPage_Game : UIPage
{
	#region Variables and Properties
	[Header("Generals")]
	[SerializeField] private GameObject GameUIGroup;
	[SerializeField] private UI_CountDown CountDown;
	[SerializeField] private UI_RectPositionTweener pausePanelTweener;
	bool pauseMenuIsOpen;

	[Header("Power Bar")]
	[SerializeField] private Image powerBarFillImage;
	[SerializeField] private RectTransform powerBarTarget;
	[SerializeField] private RectTransform perfectShotText;
	[SerializeField] private UI_RectPositionTweener perfectShotTextTweener;
	private const float powerBarTargetSizeY = 0.01f;
	private bool perfectShotTextEnabled = false;

	[Header("Top Panel")]
	[SerializeField] private UI_PlayerPanel playerPanel;
	[SerializeField] private UI_PlayerPanel AIPanel;

	[Header("FireBall Bar")]
	[SerializeField] private Image fireBallFillImage;
	[SerializeField] private Image fireBallBGImage;
	[SerializeField] private Image fireBallMultiplierBGImage;
	[SerializeField] private TextMeshProUGUI fireBallMultiplierText;
	[Space(5)]
	[SerializeField] private Color fireBallBGBaseColor;
	[SerializeField] private Color fireBallBGColorOnFire;
	[SerializeField] private Color multiplierTextBaseColor;
	[SerializeField] private Color multiplierTextColorOnFire;
	[Space(5)]
	[SerializeField] private CustomTimer timerToEmptyTheBar;
	/// <summary>
	/// This value is used to store the starting value for the empty interpolation.
	/// </summary>
	private float OnFireBarValueToEmpty;

	[Header("Notifications")]
	[SerializeField] private TextMeshProUGUI playerNotificationText;
	[SerializeField] private UI_RectPositionTweener playerNotificationTweener;
	[Space(5)]
	[SerializeField] private TextMeshProUGUI AINotificationText;
	[SerializeField] private UI_RectPositionTweener AINotificationTweener;
	[Space(5)]
	[SerializeField] private NotificationsText_SO notificationsTextData;

	[Header("Draw")]
	[SerializeField] private GameObject DrawText;
	#endregion

	#region Monobehaviours
	private void Awake()
	{
		GlobalEvents.ShotInitialized += OnShotInitialized;
		GlobalEvents.PowerBarProgressUpdated += OnPowerBarProgressUpdated;
		GlobalEvents.BallScored += OnBallScored;
		GlobalEvents.FireBallProgressUpdated += OnFireBallProgressUpdated;
		GlobalEvents.GameTimerUpadated += OnGameTimerUpdated;
		GlobalEvents.GameStarted += OnGameStarted;
		GlobalEvents.Notify += OnNotify;
		GlobalEvents.CountDownEnded += OnCountDownEnded;
		GlobalEvents.GameDraw += OnGameDraw;
		GlobalEvents.PerfectShot += OnPerfectShot;
	}
	private void OnDestroy()
	{
		GlobalEvents.ShotInitialized -= OnShotInitialized;
		GlobalEvents.PowerBarProgressUpdated -= OnPowerBarProgressUpdated;
		GlobalEvents.BallScored -= OnBallScored;
		GlobalEvents.FireBallProgressUpdated -= OnFireBallProgressUpdated;
		GlobalEvents.GameTimerUpadated -= OnGameTimerUpdated;
		GlobalEvents.GameStarted -= OnGameStarted;
		GlobalEvents.Notify -= OnNotify;
		GlobalEvents.CountDownEnded -= OnCountDownEnded;
		GlobalEvents.GameDraw -= OnGameDraw;
		GlobalEvents.PerfectShot += OnPerfectShot;
	}

	private void Update()
	{
		//FireBar
		if(timerToEmptyTheBar != null && timerToEmptyTheBar.IsActive)
		{
			timerToEmptyTheBar.Clock();
			fireBallFillImage.fillAmount = Mathf.Lerp(OnFireBarValueToEmpty, 0, timerToEmptyTheBar.Progress);
		}
	}
	#endregion

	#region Methods
	private void OnGameStarted(bool vsAI)
	{
		AIPanel.gameObject.SetActive(vsAI);

		playerPanel.Init();
		if (vsAI)
			AIPanel.Init();

		RemoveNotification(true);
		RemoveNotification(false);

		DrawText.SetActive(false);

		GameUIGroup.SetActive(false);
		CountDown.gameObject.SetActive(true);
		CountDown.StartCountDown();

		pauseMenuIsOpen = false;
		pausePanelTweener.ResetPositionToStart();

		perfectShotTextEnabled = true;
		perfectShotText.gameObject.SetActive(true);
		perfectShotTextTweener.Play(false);
	}

	private void OnCountDownEnded()
	{
		//GameTimer will start OnCountDownEnded, so if it is not started yet, the countdown is used to start the game.
		//This method will be called before the one in the script ShootingRaceHandler, because on script execution order,
		//this script will be executed before the ShootingRaceHandler (and this will be subscribed before)
		if (!ShootingRaceHandler.GameTimerActive) 
		{
			GameUIGroup.SetActive(true);
		}

		CountDown.gameObject.SetActive(false);
	}

	private void OnGameDraw()
	{
		DrawText.SetActive(true);
		Invoke("OnGameDrawInvoke", 2.5f);
	}
	private void OnGameDrawInvoke()
	{
		DrawText.SetActive(false);
		CountDown.gameObject.SetActive(true);
		CountDown.StartCountDown();
	}

	#region PowerBar
	private void OnShotInitialized(float perfectShotProgress)
	{
		powerBarFillImage.fillAmount = 0;

		powerBarTarget.anchorMin = new Vector2(powerBarTarget.anchorMin.x, perfectShotProgress - powerBarTargetSizeY / 2);
		powerBarTarget.anchorMax = new Vector2(powerBarTarget.anchorMax.x, perfectShotProgress + powerBarTargetSizeY / 2);

		if (perfectShotTextEnabled)
		{
			perfectShotText.anchorMin = new Vector2(perfectShotText.anchorMin.x, perfectShotProgress - powerBarTargetSizeY / 2);
			perfectShotText.anchorMax = new Vector2(perfectShotText.anchorMax.x, perfectShotProgress + powerBarTargetSizeY / 2);
		}
	}

	private void OnPowerBarProgressUpdated(float powerBarProgress)
	{
		powerBarFillImage.fillAmount = powerBarProgress;
	}

	private void OnPerfectShot(bool isAIBall)
	{
		if (isAIBall || !perfectShotTextEnabled)
			return;

		perfectShotText.gameObject.SetActive(false);
		perfectShotTextTweener.Stop();
		perfectShotTextEnabled = false;
	}
	#endregion

	#region PanelTop
	private void OnBallScored(bool isAI, int score, float fireBallProgressGained)
	{
		if (!isAI)
			playerPanel.SetScoreText(score);
		else
			AIPanel.SetScoreText(score);
	}

	private void OnGameTimerUpdated(float gameTimerProgress)
	{
		playerPanel.SetGameTimerFillAmount(gameTimerProgress);
		if (AIPanel.gameObject.activeInHierarchy)
			AIPanel.SetGameTimerFillAmount(gameTimerProgress);
	}
	#endregion

	#region FireBar
	private void OnFireBallProgressUpdated(bool isAI, float fireBallProgress, bool toEmpty)
	{
		if (!isAI)
		{
			if (fireBallProgress <= 0 || toEmpty)
			{
				playerPanel.EnableOnFirePanel(false);
				fireBallBGImage.color = fireBallBGBaseColor;
				fireBallMultiplierBGImage.color = fireBallBGBaseColor;
				fireBallMultiplierText.color = multiplierTextBaseColor;
			}
			else if (fireBallProgress >= 1)
			{
				playerPanel.EnableOnFirePanel(true);
				fireBallBGImage.color = fireBallBGColorOnFire;
				fireBallMultiplierBGImage.color = fireBallBGColorOnFire;
				fireBallMultiplierText.color = multiplierTextColorOnFire;
			}

			if (!toEmpty)
				fireBallFillImage.fillAmount = fireBallProgress;
			else
			{
				OnFireBarValueToEmpty = fireBallProgress;
				timerToEmptyTheBar.Start();
			}
		}
		else
		{
			if (fireBallProgress <= 0 || toEmpty)
				AIPanel.EnableOnFirePanel(false);
			else if (fireBallProgress >= 1)
				AIPanel.EnableOnFirePanel(true);
		}
	}
	#endregion

	#region Notification
	private void OnNotify(bool AINotification, NotificationType notificationType, int scoreValue)
	{
		string targetText;
		targetText = notificationsTextData.GetNotificationText(notificationType);
		targetText = targetText.Replace("_", scoreValue.ToString());

		if (AINotification && notificationType == NotificationType.LuckyBall)
			return;

		AddNotification(AINotification, targetText);
	}
	private void AddNotification(bool AINotification, string text)
	{
		if (!AINotification)
		{
			playerNotificationText.text = text;
			playerNotificationText.gameObject.SetActive(true);
			playerNotificationTweener.Play(false);
		}
		else
		{
			AINotificationText.gameObject.SetActive(true);
			AINotificationText.text = text;
			AINotificationTweener.Play(false);
		}
	}
	public void RemoveNotification(bool AINotification)
	{
		if (!AINotification)
			playerNotificationText.gameObject.SetActive(false);
		else
			AINotificationText.gameObject.SetActive(false);
	}
	#endregion

	#region Pause
	private void PauseGame(bool pause)
	{
		pausePanelTweener.Play(!pause);
		pauseMenuIsOpen = pause;
	}

	public void PauseButton()
	{
		PauseGame(!pauseMenuIsOpen);
	}
	public void ResumeButton()
	{
		PauseGame(false);
	}
	public void MainMenuButton()
	{
		pausePanelTweener.ResetPositionToStart();
		UIManager.singleInstance.BackToMainMenu();
	}
	#endregion
	#endregion
}
