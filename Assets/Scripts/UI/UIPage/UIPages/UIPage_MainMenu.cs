﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPage_MainMenu : UIPage
{
	#region Variables and Properties
	[SerializeField] private GameObject ButtonsParent;
	[SerializeField] private GameObject optionPanel;
	[SerializeField] private Slider musicSlider;
	[SerializeField] private Slider SFXSlider;
	#endregion

	#region Monobehaviours
	private void OnEnable()
	{
		OpenOptionPanel(false);
	}

	#endregion

	#region Methods
	public void OpenOptionPanel(bool open)
	{
		ButtonsParent.SetActive(!open);
		optionPanel.SetActive(open);
		if (open)
			InitSliders();
	}

	public void OnMusicSliderChanged()
	{
		AudioManager.singleInstance.SetBusVolume(BusType.Music, musicSlider.value);
	}
	public void OnSFXSliderChanged()
	{
		AudioManager.singleInstance.SetBusVolume(BusType.SFX, SFXSlider.value);
	}
	public void InitSliders()
	{
		musicSlider.value = SaveData.MusicVolume;
		SFXSlider.value = SaveData.SFXVolume;
	}
	#endregion
}
