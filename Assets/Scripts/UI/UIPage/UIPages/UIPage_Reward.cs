﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIPage_Reward : UIPage
{
	#region Variables and Properties
	[Header("Solo Mode")]
	[SerializeField] private GameObject soloModePanel;
	[SerializeField] private TextMeshProUGUI soloModeScoreText;

	[Header("VS AI Mode")]
	[SerializeField] private GameObject vsAIModePanel;
	[SerializeField] private TextMeshProUGUI playerScoreText;
	[SerializeField] private GameObject playerWinnerText;
	[SerializeField] private TextMeshProUGUI AIScoreText;
	[SerializeField] private GameObject AIWinnerText;

	private bool isVsAIMode;
	#endregion

	#region Monobehaviours
	private void Awake()
	{
		GlobalEvents.SoloModeGameEnded += OnSoloModeGameEnded;
		GlobalEvents.VsAIModeGameEnded += OnVsAIModeGameEnded;
	}
	private void OnDestroy()
	{
		GlobalEvents.SoloModeGameEnded -= OnSoloModeGameEnded;
		GlobalEvents.VsAIModeGameEnded -= OnVsAIModeGameEnded;
	}
	#endregion

	#region Methods
	private void OnSoloModeGameEnded(float playerScore)
	{
		UIManager.singleInstance.SetPage<UIPage_Reward>();

		soloModePanel.SetActive(true);
		vsAIModePanel.SetActive(false);

		soloModeScoreText.text = playerScore.ToString();

		isVsAIMode = false;
	}
	
	private void OnVsAIModeGameEnded(bool playerWon, float playerScore, float AIScore)
	{
		UIManager.singleInstance.SetPage<UIPage_Reward>();

		soloModePanel.SetActive(false);
		vsAIModePanel.SetActive(true);

		playerWinnerText.SetActive(playerWon);
		AIWinnerText.SetActive(!playerWon);

		playerScoreText.text = playerScore.ToString();
		AIScoreText.text = AIScore.ToString();

		isVsAIMode = true;
	}

	public void PlayAgainButton()
	{
		if (isVsAIMode)
			UIManager.singleInstance.VSPlay();
		else
			UIManager.singleInstance.SoloPlay();
	}
	#endregion
}
