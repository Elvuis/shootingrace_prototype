﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallShooter : MonoBehaviour
{
	#region Variables and Properties
	Transform m_Transform;
	protected Transform Transform { get { if (!m_Transform) m_Transform = transform; return m_Transform; } }

	private ShotDataDistanceKvp m_CurrentShotData;
	public ShotData_SO CurrentShotData
	{
		get
		{
			if (m_CurrentShotData == null || m_CurrentShotData.ShotDistance != CurrentShotDistance)
				m_CurrentShotData = ShootingRaceHandler.singleInstance.GetShotDataDistanceKvp(CurrentShotDistance);
			return m_CurrentShotData.ShotData;
		}
	}

	public ShotDistances CurrentShotDistance { get; protected set; }
	public int CurrentShotIndex { get; protected set; }

	protected Ball ball;

	public bool ShotInProgress { get; protected set; } = false;
	protected bool CanShot => !ShotInProgress && ShootingRaceHandler.GameTimerActive;

	protected float powerBarProgress;
	protected float horizontalAngle;

	private float fireBallProgress;
	private CustomTimer fireBallTimer;
	public bool isOnFire => fireBallTimer.IsActive;

	private AudioSourceHandler OnFireLoop = null;
	#endregion

	#region Monobehaviours
	protected virtual void OnEnable()
	{
	}
	protected virtual void OnDisable()
	{
		ball = null;
	}

	protected virtual void Update()
	{
		if(fireBallTimer != null && fireBallTimer.IsActive)
		{
			fireBallTimer.Clock();
			fireBallProgress = 1 - fireBallTimer.Progress;
			GlobalEvents.FireBallProgressUpdated?.Invoke(this is AIPlayer, fireBallProgress, false);

			//Sound
			if(fireBallTimer.Expired && this is Player)
			{
				if (OnFireLoop != null)
				{
					OnFireLoop?.Stop();
					AudioManager.singleInstance.Play(PoolTags.FireExtinguish);
				}
				OnFireLoop = null;
			}
		}
		else if(fireBallProgress > 0) //Slow Empty the bar overtime.
		{
			fireBallProgress -= ShootingRaceHandler.fireBallData.FireBallProgressLossValue;
			GlobalEvents.FireBallProgressUpdated?.Invoke(this is AIPlayer, fireBallProgress, false);
		}
	}
	#endregion

	#region Methods
	public void Init()
	{
		OnFireLoop = null;
		ResetCurrentShot();
		fireBallTimer = new CustomTimer(ShootingRaceHandler.fireBallData.FireBallBonusDuration);
		EmptyFireBallProgress();
		InitShot(false);
	}

	public void ResetCurrentShot()
	{
		CurrentShotIndex = 0;
		CurrentShotDistance = ShotDistances.Near;
	}

	public virtual void InitShot(bool scored, bool isLuckyShot = false)
	{
		if (scored)
		{
			CurrentShotIndex++;
			if(CurrentShotIndex >= CurrentShotData.ShotCountFromThisDistance)
			{
				CurrentShotIndex = 0;
				switch (CurrentShotDistance)
				{
					case ShotDistances.Near:
						CurrentShotDistance = ShotDistances.Middle;
						break;
					case ShotDistances.Middle:
						CurrentShotDistance = ShotDistances.Far;
						break;
					case ShotDistances.Far:
						//restart Far distances
						break;
				}
			}
		}

		//TMP; waiting for view
		Transform.position = GetShotPosition();

		ball = PoolManager.singleInstance.Pull(PoolTags.Ball).GetComponent<Ball>();
		ball.Init(GetShotPosition(), isLuckyShot, fireBallTimer.IsActive, this is AIPlayer);

		ShotInProgress = false;
		powerBarProgress = 0;
		horizontalAngle = 0;
	}

	protected void Shoot()
	{
		ball.Shoot(CurrentShotData.ShotForce, GetShotDirection());
		ShotInProgress = true;
		ball = null;
	}

	private Vector3 GetShotDirection()
	{
		Vector3 result = Basket.singleInstance.GetDirectionFromTargetPosition(ball.Transform.position);

		float interpolationValue;
		float targetForwardOffest;
		float targetVerticality;

		if (powerBarProgress > CurrentShotData.PerfectShotProgress)
		{
			//Get the target interpolation value
			interpolationValue = Mathf.InverseLerp(CurrentShotData.PerfectShotProgress, 1, powerBarProgress);
			//Get the target forward offset
			targetForwardOffest = Mathf.Lerp(CurrentShotData.PerfectShotDirectionalInfluence.shotForwardOffset,
											 CurrentShotData.StrongShotDirectionalInfluence.shotForwardOffset, 
											 interpolationValue);
			//Get the target verticality
			targetVerticality = Mathf.Lerp(CurrentShotData.PerfectShotDirectionalInfluence.shotVerticality,
										   CurrentShotData.StrongShotDirectionalInfluence.shotVerticality, 
										   interpolationValue);
		}
		else
		{
			//Get the target interpolation value
			interpolationValue = Mathf.InverseLerp(0, CurrentShotData.PerfectShotProgress, powerBarProgress);
			//Get the target forward offset
			targetForwardOffest = Mathf.Lerp(CurrentShotData.LightShotDirectionalInfluence.shotForwardOffset,
											 CurrentShotData.PerfectShotDirectionalInfluence.shotForwardOffset,
											 interpolationValue);
			//Get the target verticality
			targetVerticality = Mathf.Lerp(CurrentShotData.LightShotDirectionalInfluence.shotVerticality,
										   CurrentShotData.PerfectShotDirectionalInfluence.shotVerticality,
										   interpolationValue);
		}

		result *= targetForwardOffest;
		if(horizontalAngle != 0)
		{
			//The horizontalAngle is influenced by the powerBar progress. Lower the powerbar, higher the horizontal influence.
			float horizontalAngleScaler = 1 - Mathf.Clamp(powerBarProgress, 0, 0.9f); //if there are an horizontal influence, it cannot be 0, so the powerbar value is used clamped.
			result.RotateDirection(Vector3.up, horizontalAngle * horizontalAngleScaler);
		}
		result.y = targetVerticality;

		
		return result;
	}

	public Vector3 GetShotPosition()
	{
		if (CurrentShotIndex >= CurrentShotData.ShotCountFromThisDistance)
		{
#if UNITY_EDITOR || DEVELOPMENT_BUILD
			Debug.LogError("You just requested a shot that doesn't exists. Shot requested: " + CurrentShotIndex + "; there are only " + CurrentShotData.ShotCountFromThisDistance + " shots.");
#endif
			return Vector3.zero;
		}

		Vector3 shotPositionAtDistance = Basket.singleInstance.Transform.position;
		shotPositionAtDistance.z += CurrentShotData.BaseForwardDistanceFromBasket;

		//Set orientation for player
		int rotationOrientation = ShootingRaceHandler.singleInstance.PlayerStartsRotatingClockwise ? 1 : -1;
		//if this is the AI, use opposite orientation
		if (this is AIPlayer)
			rotationOrientation *= -1;

		return shotPositionAtDistance.RotateAround(Basket.singleInstance.Transform.position, Vector3.up, CurrentShotData.RotationDegrees[CurrentShotIndex] * rotationOrientation);
	}

	public virtual void AddFireBallProgress(float progress)
	{
		fireBallProgress += progress;
		if(fireBallProgress >= 1)
		{
			fireBallProgress = 1;
			fireBallTimer.Start();

			//Sound
			if (this is Player)
				OnFireLoop = AudioManager.singleInstance.Play(PoolTags.FireLoop);
		}
		GlobalEvents.FireBallProgressUpdated?.Invoke(this is AIPlayer, fireBallProgress, false);

	}

	public void EmptyFireBallProgress()
	{
		//The current fireball progress is needed on ui, so the event will be called before to reset it
		GlobalEvents.FireBallProgressUpdated?.Invoke(this is AIPlayer, fireBallProgress, true);
		fireBallProgress = 0;
		fireBallTimer.Stop();

		//Sound
		if (this is Player)
		{
			if (OnFireLoop != null)
			{
				AudioManager.singleInstance.Play(PoolTags.FireExtinguish);
				OnFireLoop.Stop();
			}
			OnFireLoop = null;
		}
	}
	#endregion
}
