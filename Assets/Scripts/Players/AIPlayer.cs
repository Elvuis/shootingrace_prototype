﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPlayer : BallShooter
{
	#region Variables and Properties
	[Header("AI Data")]
	[Tooltip("Minimum time to wait before the AI shoots")]
	[SerializeField] private float minDurationToShot;
	[Tooltip("Maximum time to wait before the AI shoots")]
	[SerializeField] private float maxDurationToShot;
	private CustomTimer timerToShot;

	[Header("Standard power and direction")]
	[Tooltip("It will be subtracted from the current PerfectShot progress to set a minimum power value")]
	[Range(0, 1)]
	[SerializeField] private float standardMinPowerFromPerfectShot;
	[Tooltip("It will be added to the current PerfectShot  progress to set a maximum power value")]
	[Range(0, 1)]
	[SerializeField] private float standardMaxPowerFromPerfectShot;

	[Space(5)]
	[Tooltip("Positive or negative max angle degrees of the shot")]
	[Min(0)]
	[SerializeField] private float standardHorizontalAngleThreshold;

	[Header("Power and direction on disadvantage")]
	[Tooltip("If AI score is lesser than (PlayerScore - thisValue) disadvantage values will be used.")]
	[Min(0)] 
	[SerializeField] private int minScoreDifferenceToDisadvantageShots;

	[Space(5)]
	[Tooltip("On disadvantage state, it will be subtracted from the current PerfectShot progress to set a minimum power value")]
	[Range(0, 1)]
	[SerializeField] private float disadvantageMinPowerFromPerfectShot;
	[Tooltip("On disadvantage state, it will be added to the current PerfectShot  progress to set a maximum power value")]
	[Range(0, 1)]
	[SerializeField] private float disadvantageMaxPowerFromPerfectShot;

	[Space(5)]
	[Tooltip("Positive or negative max angle degrees of the shot on disadvantage state")]
	[Min(0)]
	[SerializeField] private float disagvantageHorizontalAngleThreshold;

	private bool DisadvantageState => ShootingRaceHandler.singleInstance.AIPlayerScore <= ShootingRaceHandler.singleInstance.PlayerScore - minScoreDifferenceToDisadvantageShots;
	private float MinPowerFromPerfectShot => DisadvantageState ? disadvantageMinPowerFromPerfectShot : standardMinPowerFromPerfectShot;
	private float MaxPowerFromPerfectShot => DisadvantageState ? disadvantageMaxPowerFromPerfectShot : standardMaxPowerFromPerfectShot;
	private float HorizontalAngleThreshold => DisadvantageState ? disagvantageHorizontalAngleThreshold : standardHorizontalAngleThreshold;
	#endregion

	#region Monobehaviours
	protected override void Update()
	{
		base.Update();

		if (CanShot)
		{
			if (timerToShot != null && timerToShot.IsActive)
				timerToShot.Clock();
		}
	}
	#endregion

	#region Methods
	public override void InitShot(bool scored, bool isLuckyShot = false)
	{
		base.InitShot(scored, isLuckyShot);
		timerToShot = new CustomTimer(Random.Range(minDurationToShot, maxDurationToShot));
		timerToShot.ClearTimerEndedEvent();
		timerToShot.AddUnityEvent(AIShoot);
		timerToShot.Start();
	}

	private void AIShoot()
	{
		powerBarProgress = Random.Range(CurrentShotData.PerfectShotProgress - MinPowerFromPerfectShot, CurrentShotData.PerfectShotProgress + MaxPowerFromPerfectShot);
		horizontalAngle = Random.Range(-HorizontalAngleThreshold, HorizontalAngleThreshold);
		Shoot();
	}
	#endregion
}
