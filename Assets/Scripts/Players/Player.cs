﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : BallShooter
{
	#region Variables and Properties
	[Header("Player Input Data")]
	[Tooltip("This value will be multiplied to the horizontal input movement to add horizontality to the shot." +
			 " i.e. if it equals to 1, an horizontal movement through the full screen would result to an horizontal angle of 1")]
	[SerializeField] private float horizontalInfluenceMultiplier;
	[Tooltip("This value will be multiplied to the actual upper input movement to fill the PowerBar." +
			 " i.e. if it equals to 1, a vertical movement through the full screen would be needed to fill the PowerBar")]
	[SerializeField] private float powerBarFillMultiplier;

	[Space(10)]
	[SerializeField] private CustomTimer shotInputTimer;

	private Vector3 lastInputPosition;

	[Header("Debug")]
	[SerializeField] private bool customShot;
	[Range(0, 1)]
	[SerializeField] private float customShotProgress;
	[Range(-30, 30)]
	[SerializeField] private float customHorizontalAngle;
	#endregion

	#region Monobehaviours
	protected override void OnEnable()
	{
		base.OnEnable();

		//Inputs events
		GlobalEvents.TouchStarted += OnTouchStarted;
		GlobalEvents.TouchMoved += OnTouchMoved;
		GlobalEvents.TouchEnded += OnTouchEnded;
		GlobalEvents.MouseAndKeyboardInputRetrived += OnMouseAndKeyboardInputRetrived;
	}
	protected override void OnDisable()
	{
		base.OnDisable();

		//Inputs events
		GlobalEvents.TouchStarted -= OnTouchStarted;
		GlobalEvents.TouchMoved -= OnTouchMoved;
		GlobalEvents.TouchEnded -= OnTouchEnded;
		GlobalEvents.MouseAndKeyboardInputRetrived -= OnMouseAndKeyboardInputRetrived;
	}

	protected override void Update()
	{
		base.Update();

		if (shotInputTimer.IsActive)
			shotInputTimer.Clock();
	}
    #endregion

    #region Methods
    #region Inputs
    private void OnTouchStarted(Vector3 touchPosition)
	{
		InputEnter(touchPosition);
	}
	private void OnTouchMoved(Vector3 touchPosition)
	{
		InputHold(touchPosition);
	}
	private void OnTouchEnded(Vector3 touchPosition)
	{
		InputReleased(touchPosition);
	}
	private void OnMouseAndKeyboardInputRetrived(KeyCode key, InputTypes inputType)
	{
		switch (key)
		{
			case KeyCode.Mouse0:
			{
				switch (inputType)
				{
					case InputTypes.Press:
					{
						InputEnter(InputManager.ViewportMousePosition);
						break;
					}
					case InputTypes.Hold:
					{
						InputHold(InputManager.ViewportMousePosition);
						break;
					}
					case InputTypes.Release:
					{
						InputReleased(InputManager.ViewportMousePosition);
						break;
					}
				}
				break;
			}
		}
	}

    /// <param name="inputPosition">Input position in Viewport.</param>
    private void InputEnter(Vector3 inputPosition)
	{
		if (!CanShot)
			return;

		if (customShot)
		{
			powerBarProgress = customShotProgress;
			horizontalAngle = customHorizontalAngle;
			Shoot();
			return;
		}

		lastInputPosition = inputPosition;
		//shotInputTimer.Start();
	}
	/// <param name="newInputPosition">Input position in Viewport.</param>
	private void InputHold(Vector3 newInputPosition)
	{
		if (!CanShot)
			return;

		//VerticalCheck
		if (newInputPosition.y > lastInputPosition.y)
		{
			if (!shotInputTimer.IsActive)
				shotInputTimer.Start();

			//Using viewport values as input, if the powerBarFillFactor is 1, the movement required to 
			//completely fill the powerbar would be from the bottom of the screen to the top; if powerBarFillFactor
			//is 2, half sreen would be needed to fill the powerBar
			powerBarProgress += (newInputPosition.y - lastInputPosition.y) * powerBarFillMultiplier;
			GlobalEvents.PowerBarProgressUpdated?.Invoke(powerBarProgress);
		}

		if (shotInputTimer.IsActive)
		{
			//HorizontalCheck
			if (newInputPosition.x != lastInputPosition.x)
			{
				if ((newInputPosition.x > lastInputPosition.x && horizontalAngle < CurrentShotData.MaxHorizontalShotAngle) || //Movement toward right and horizontalInfluence lesser than max positive horizontal influence
					newInputPosition.x < lastInputPosition.x && horizontalAngle > -CurrentShotData.MaxHorizontalShotAngle) //Movement toward left and horizontalInfluence higher than max negative horizontal influence
				{
					horizontalAngle += (newInputPosition.x - lastInputPosition.x) * horizontalInfluenceMultiplier;
					horizontalAngle = Mathf.Clamp(horizontalAngle, -CurrentShotData.MaxHorizontalShotAngle, CurrentShotData.MaxHorizontalShotAngle);
				}
			}

			GlobalEvents.InputMoved(newInputPosition);
		}

		lastInputPosition = newInputPosition;
	}
	/// <param name="inputPosition">Input position in Viewport.</param>
	private void InputReleased(Vector3 inputPosition)
	{
		if (!CanShot || !shotInputTimer.IsActive)
			return;

		ShootTheBall();
	}
	#endregion

	public override void InitShot(bool scored, bool isLuckyShot = false)
	{
		base.InitShot(scored, isLuckyShot);
		CameraHandler.singleInstance.OnInitShot(ball.Transform.position);
		GlobalEvents.ShotInitialized?.Invoke(CurrentShotData.PerfectShotProgress);

		//Sound
		if (isLuckyShot)
			AudioManager.singleInstance.Play(PoolTags.LuckyShot);
	}

	public void ShootTheBall()
	{
		shotInputTimer.Stop();
		Shoot();

		CameraHandler.singleInstance.OnPlayerShoot();
		GlobalEvents.DisableInputTrailRenderer?.Invoke();
	}
	#endregion
}
