﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
	#region Variables and Properties
	private List<AudioSourceHandler> activeSources = new List<AudioSourceHandler>();
	#endregion

	#region Monobehaviours
	private void Start()
	{
		PlayBGMusicIntro();
	}
	private void OnEnable()
	{
		GlobalEvents.ClipStopped += OnClipStopped;
	}
	private void OnDisable()
	{
		GlobalEvents.ClipStopped -= OnClipStopped;
	}
	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			StopAllSoundEffects();
		}
	}
	#endregion

	#region Methods
	public AudioSourceHandler Play(string tag)
	{
		AudioSourceHandler result = PoolManager.singleInstance.Pull(tag, transform.position).GetComponent<AudioSourceHandler>();
		result.Play();
		activeSources.Add(result);
		return result;
	}

	public static float GetBusVolume(BusType busType)
	{
		switch (busType)
		{
			case BusType.Music:
				return SaveData.MusicVolume;
			case BusType.SFX:
				return SaveData.SFXVolume;
		}
		return 0;
	}
	
	public void SetBusVolume(BusType busType, float volume)
	{
		switch (busType)
		{
			case BusType.Music:
				 SaveData.MusicVolume = volume;
				break;
			case BusType.SFX:
				 SaveData.SFXVolume = volume;
				break;
		}

		GlobalEvents.VolumeChanged?.Invoke();
	}

	public void StopAllSoundEffects()
	{
		for(int i = activeSources.Count - 1; i>= 0; i--)
		{
			if(activeSources[i].BusType == BusType.SFX)
				activeSources[i].Stop();
		}
	}

	private void OnClipStopped(AudioSourceHandler source)
	{
		if (activeSources.Contains(source))
			activeSources.Remove(source);
	}

	private void PlayBGMusicIntro()
	{
		AudioSourceHandler BGMusicIntro = Play(PoolTags.BGMusicIntro);
		BGMusicIntro.OnClipEnded.AddListener(() => { Play(PoolTags.BGMusicLoop); });
	}
	#endregion
}
