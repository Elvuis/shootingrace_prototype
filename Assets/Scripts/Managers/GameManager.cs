﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
	#region Variables and Properties

	#endregion

	#region Monobehaviours
	#endregion

#region Methods
	public void StartShootingRace(bool vsAI)
	{
		GlobalEvents.GameStarted?.Invoke(vsAI);
	}
#endregion
}