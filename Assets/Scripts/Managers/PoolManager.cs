﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PoolManager : Singleton<PoolManager>
{
    #region Variables and Properties
    public List<PoolObjectsList> items;
	private Dictionary<string, GameObject> poolParents = new Dictionary<string, GameObject>();
	private Dictionary<string, List<GameObject>> inactivePooledObjects = new Dictionary<string, List<GameObject>>();
    private Dictionary<string, List<GameObject>> activePooledObjects = new Dictionary<string, List<GameObject>>();
    private Dictionary<string, int> expandedPoolObjectsCount = new Dictionary<string, int>();
    #endregion

    #region MonoBehaviours
    protected override void Awake()
    {
        base.Awake();

        //System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        //stopwatch.Start();
        for (int i = 0; i< items.Count; i++)
        {
            PoolObjectsList currentItem = items[i];

            GameObject itemParent = new GameObject();
            itemParent.name = currentItem.category;
            itemParent.transform.SetParent(transform);
            itemParent.transform.localPosition = Vector3.zero;

            for(int j=0; j< currentItem.poolObjects.Length; j++) //each item has an internal list of PoolObjects.
            {
                PoolObject_SO currentPoolObject = currentItem.poolObjects[j];
                List<GameObject> objects = new List<GameObject>();

                GameObject objParent = new GameObject();				
                objParent.name = currentPoolObject.poolTag;
                objParent.transform.SetParent(itemParent.transform);
                objParent.transform.localPosition = Vector3.zero;
				poolParents.Add(objParent.name, objParent);

				for (int k = 0; k < currentPoolObject.amountToPool; k++)
                {
                    GameObject obj = Instantiate(currentPoolObject.objectToPool, objParent.transform);
                    obj.transform.localPosition = Vector3.zero;
                    obj.SetActive(false);
                    objects.Add(obj);
                }

                inactivePooledObjects.Add(currentPoolObject.poolTag, objects);
                activePooledObjects.Add(currentPoolObject.poolTag, new List<GameObject>());
            }
        }
        //stopwatch.Stop();
        //Debug.Log(stopwatch.ElapsedMilliseconds + " milliseconds; " + stopwatch.ElapsedTicks + " ticks");
    }
    #endregion

    #region Methods
    /// <summary>
    /// Get a GameObject from the pool
    /// </summary>
    /// <param name="tag"></param>
    /// <param name="position"></param>
    /// <returns></returns>
    public GameObject Pull(string tag, Vector3 position = new Vector3())
    {
        if (inactivePooledObjects.ContainsKey(tag))
        {
            if (inactivePooledObjects[tag].Count > 0)
            {
                GameObject obj = inactivePooledObjects[tag][0];
                obj.SetActive(true);
                activePooledObjects[tag].Add(obj);
                inactivePooledObjects[tag].Remove(obj);
                if(position != null)
                    obj.transform.position = position;
                return obj;
            }
            else //check if that gameObject should expand
            {
                for (int i = 0; i < items.Count; i++)
                {
                    PoolObjectsList currentItem = items[i];

                    for (int j = 0; j< currentItem.poolObjects.Length; j++)
                    {
                        PoolObject_SO currentPoolObject = currentItem.poolObjects[j];

                        if (currentPoolObject.poolTag == tag && currentPoolObject.shouldExpand)
                        {
                            GameObject obj = Instantiate(currentPoolObject.objectToPool, GameObject.Find(currentPoolObject.poolTag).transform);
                            activePooledObjects[tag].Add(obj);
                            if (position != null)
                                obj.transform.position = position;

                            if (!expandedPoolObjectsCount.ContainsKey(currentPoolObject.poolTag))
                                expandedPoolObjectsCount.Add(currentPoolObject.poolTag, 0);

                            expandedPoolObjectsCount[currentPoolObject.poolTag]++;
#if UNITY_EDITOR
                            Debug.LogError(currentPoolObject.poolTag + " expanded. This PoolObject was expanded " + expandedPoolObjectsCount[currentPoolObject.poolTag] + " times");
#endif
                            return obj;
                        }
                    }
                }
            }
        }
        else //if there isn't a pool list with "tag"
        {
#if UNITY_EDITOR
            Debug.LogError("PoolManager: an object with '" + tag + "' tag does not exist");
#endif
            return null;
        }

        //if the "tag" pool is empty
#if UNITY_EDITOR
        Debug.LogError("PoolManager: the '" + tag + "' pool is empty, and ShouldExpand is false.");
#endif
        return null;
    }

    /// <summary>
    /// Return the list with all the pooled Object.
    /// </summary>
    /// <param name="tag"></param>
    /// <returns></returns>
    public List<GameObject> GetActivePoolObjects(string tag)
    {
        if (inactivePooledObjects.ContainsKey(tag))
        {
            return activePooledObjects[tag];
        }
        return null;
    }
    
    /// <summary>
    /// Return the GameObject back to his pool.
    /// </summary>
    /// <param name="tag"></param>
    /// <param name="obj"></param>
    public void Push(string tag, GameObject obj)
    {
        obj.SetActive(false);
        if (activePooledObjects[tag].Contains(obj))
            activePooledObjects[tag].Remove(obj);
        if(!inactivePooledObjects[tag].Contains(obj))
            inactivePooledObjects[tag].Add(obj);
		obj.transform.SetParent(poolParents[tag].transform);
        obj.transform.localPosition = Vector3.zero;
    }

    /// <summary>
    /// Return a list of GameObject back to their pool.
    /// </summary>
    /// <param name="tag"></param>
    /// <param name="obj"></param>
    public void PushObjectsList(string tag, List<GameObject> objs)
    {
        for(int i = objs.Count -1; i >= 0; i--)
            Push(tag, objs[i]);
    }
    /// <summary>
    /// Return an Array of GameObject back to their pool.
    /// </summary>
    /// <param name="tag"></param>
    /// <param name="obj"></param>
    public void PushObjectsArray(string tag, GameObject[] objs)
    {
        for(int i = objs.Length - 1; i >= 0; i--)
            Push(tag, objs[i]);
    }
	#endregion
}
