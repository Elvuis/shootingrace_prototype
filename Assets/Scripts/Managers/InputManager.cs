﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
	#region Variables and Properties
	[SerializeField] protected bool MouseAndKeyboardInputEnabled;
	[SerializeField] protected bool TouchInputEnabled;

	public Touch CurrentFrameTouch { get; protected set; }
	public static Vector3 ViewportMousePosition => Camera.main.ScreenToViewportPoint(Input.mousePosition);
	#endregion
	
	#region Monobehaviours
	private void Update()
	{
		CheckTouchInput();
		CheckMouseAndKeyboardInput();
	}
	#endregion

	#region Methods
	private void CheckTouchInput()
	{
		if (!TouchInputEnabled || Input.touchCount <= 0)
			return;

		CurrentFrameTouch = Input.GetTouch(0);

		switch (CurrentFrameTouch.phase)
		{
			case TouchPhase.Began:
				GlobalEvents.TouchStarted?.Invoke(CurrentFrameTouch.PositionInViewport());
				break;
			case TouchPhase.Stationary:
				GlobalEvents.TouchStationary?.Invoke(CurrentFrameTouch.PositionInViewport());
				break;
			case TouchPhase.Moved:
				GlobalEvents.TouchMoved?.Invoke(CurrentFrameTouch.PositionInViewport());
				break;
			case TouchPhase.Ended:
			case TouchPhase.Canceled:
				GlobalEvents.TouchEnded?.Invoke(CurrentFrameTouch.PositionInViewport());
				break;
		}
	}

	private void CheckMouseAndKeyboardInput()
	{
		if (!MouseAndKeyboardInputEnabled)
			return;

		InputTypes targetInputType;
		//Keyboard and Mouse KeyCodes goes from 0 to "Mouse6" value
		for (int i = 0; i <= (int)KeyCode.Mouse6; i++)
		{
			if (GetInputType((KeyCode)i, out targetInputType))
				GlobalEvents.MouseAndKeyboardInputRetrived?.Invoke((KeyCode)i, targetInputType);
		}
	}
	/// <summary>
	/// Returns true if the TargetKeyCode is pressed, held or released.
	/// </summary>
	private bool GetInputType(KeyCode targetKeyCode, out InputTypes inputType)
	{
		if (Input.GetKeyDown(targetKeyCode))
		{
			inputType = InputTypes.Press;
			return true;
		}
		else if (Input.GetKey(targetKeyCode))
		{
			inputType = InputTypes.Hold;
			return true;
		}
		else if (Input.GetKeyUp(targetKeyCode))
		{
			inputType = InputTypes.Release;
			return true;
		}

		inputType = InputTypes.None;
		return false;
	}
	#endregion
}
