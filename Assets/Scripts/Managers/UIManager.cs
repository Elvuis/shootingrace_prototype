﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
	#region Variables and Properties
	private UIPage[] m_Pages = null;
	public UIPage[] Pages { get { if (m_Pages == null) m_Pages = GetComponentsInChildren<UIPage>(); return m_Pages; } }
	#endregion

	#region Monobehaviours
	private void Start()
	{
		Init();
	}
	#endregion

	#region Methods
	public void Init()
	{
		SetPage<UIPage_MainMenu>();
	}

	public void AddPage<T>() where T : UIPage
	{
		for (int i = 0; i < Pages.Length; i++)
		{
			if (Pages[i] is T)
			{
				Pages[i].gameObject.SetActive(true);
				return;
			}
		}
	}
	public void RemovePage<T>() where T : UIPage
	{
		for (int i = 0; i < Pages.Length; i++)
		{
			if (Pages[i] is T)
			{
				Pages[i].gameObject.SetActive(false);
				return;
			}
		}
	}
	public void SetPage<T>() where T : UIPage
	{
		for (int i = 0; i < Pages.Length; i++)
		{
			if (Pages[i] is T)
				Pages[i].gameObject.SetActive(true);
			else
				Pages[i].gameObject.SetActive(false);
		}
	}

	public void SoloPlay()
	{
		SetPage<UIPage_Game>();
		GameManager.singleInstance.StartShootingRace(false);
	}
	public void VSPlay()
	{
		SetPage<UIPage_Game>();
		GameManager.singleInstance.StartShootingRace(true);
	}
	public void Quit()
	{
		Application.Quit();
	}
	public void BackToMainMenu()
	{
		GlobalEvents.BackToMainMenu?.Invoke();
		SetPage<UIPage_MainMenu>();
	}
	#endregion
}
