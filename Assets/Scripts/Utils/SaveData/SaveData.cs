﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveData
{
    /* EXAMPLES
    public static bool ExampleBool
    {
        get { return PlayerPrefs.GetInt("ExampleBool", 0) == 1; }
        set { PlayerPrefs.SetInt("ExampleBool", value == true ? 1 : 0); }
    }
    public static int ExampleInt
    {
        get { return PlayerPrefs.GetInt("ExampleInt", 0); }
        set { PlayerPrefs.SetInt("ExampleInt", value); }
    }
    public static float ExampleFloat
    {
        get { return PlayerPrefs.GetFloat("ExampleFloat", 0); }
        set { PlayerPrefs.SetFloat("ExampleFloat", value); }
    }
    public static string ExampleString
    {
        get { return PlayerPrefs.GetString("ExampleString", "defaultValue"); }
        set { PlayerPrefs.SetString("ExampleString", value); }
    }
    */

    #region Audio
    public static float MusicVolume
    {
        get { return PlayerPrefs.GetFloat("MusicVolume", 0.5f); }
        set { PlayerPrefs.SetFloat("MusicVolume", Mathf.Clamp01(value)); }
    }
    public static float SFXVolume
    {
        get { return PlayerPrefs.GetFloat("SFXVolume", 0.75f); }
        set { PlayerPrefs.SetFloat("SFXVolume", Mathf.Clamp01(value)); }
    }
    #endregion
}
