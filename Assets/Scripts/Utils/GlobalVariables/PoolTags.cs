﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PoolTags
{
    public static string Ball = "Ball";

    //Sounds
    public static string BGMusicIntro = "BGMusicIntro";
    public static string BGMusicLoop = "BGMusicLoop";
    public static string BallBounceBackboard = "BallBounceBackboard";
    public static string BallBounceGround = "BallBounceGround";
    public static string BallScored = "BallScored";
    public static string CountDownBeep = "CountDownBeep";
    public static string CountDownEnd = "CountDownEnd";
    public static string FireLoop = "FireLoop";
    public static string FireExtinguish = "FireExtinguish";
    public static string GameTimerEnded = "GameTimerEnded";
    public static string TimerEnding = "TimerEnding";
    public static string LuckyBallScored = "LuckyBallScored";
    public static string LuckyShot = "LuckyShot";
    public static string UIButton = "UIButton";

}
