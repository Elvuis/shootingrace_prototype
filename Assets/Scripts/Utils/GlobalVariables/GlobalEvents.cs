﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalEvents
{
    #region Input
    /// <summary>
    /// Called in the first frame the player touched the screen.
    /// </summary>
    public static Action<Vector3> TouchStarted;
	/// <summary>
	/// Called if the player has not moved its finger in the screen.
	/// </summary>
	public static Action<Vector3> TouchStationary;
	/// <summary>
	/// Called if the player moved its finger in the screen.
	/// </summary>
	public static Action<Vector3> TouchMoved;
	/// <summary>
	/// Called in the frame in which the player released the screen.
	/// </summary>
	public static Action<Vector3> TouchEnded;

	/// <summary>
	/// Called if a mouse or keyboard input was pressed, held or released.
	/// </summary>
	public static Action<KeyCode, InputTypes> MouseAndKeyboardInputRetrived;
	#endregion

	#region Gameplay
	public static Action CountDownEnded;
	/// <summary>
	/// Param is VsAI.
	/// </summary>
	public static Action<bool> GameStarted;
	/// <summary>
	/// Param1 is Player won; Param2 is PlayerScore; Param3 is AIScore
	/// </summary>
	public static Action<bool, float, float> VsAIModeGameEnded;
	/// <summary>
	/// Param is PlayerScore.
	/// </summary>
	public static Action<float> SoloModeGameEnded;
	public static Action GameDraw;

	/// <summary>
	/// Called when the ball entered in the basket.
	/// Param1 is "IsAIBall"; Param2 is "Score"; Param3 is "FireBallProgressGained".
	/// </summary>
	public static Action<bool, int, float> BallScored;
	/// <summary>
	/// Called when the Ball touches the ground.
	/// Param1 is "IsAIBall"; Param2 is "ScoredThisShot".
	/// </summary>
	public static Action<bool, bool> ShotCompleted;
	/// <summary>
	/// Called if a Player just scored by touching the Backboard while its bonus was active.
	/// </summary>
	public static Action BackboardBonusScored;
	public static Action EnableBackboardBonus;
	/// <summary>
	/// Param is IsAIBall.
	/// </summary>
	public static Action<bool> PerfectShot;
	#endregion

	#region UI
	//Trail Renderer
	public static Action DisableInputTrailRenderer;
	/// <summary>
	/// Param is input position in Viewport.
	/// This event is called to set the input trail renderer position.
	/// </summary>
	public static Action<Vector3> InputMoved;

	//GamePage
	/// <summary>
	/// Param is Game Timer progress.
	/// </summary>
	public static Action<float> GameTimerUpadated;
	/// <summary>
	/// Param is new PerfectShotProgress.
	/// </summary>
	public static Action<float> ShotInitialized;
	/// <summary>
	/// Param is current PowerBar progress.
	/// </summary>
	public static Action<float> PowerBarProgressUpdated;
	/// <summary>
	/// Param1 is IsAI, Param2 is FireBall progress, Param3 is ToEmpty.
	/// </summary>
	public static Action<bool, float, bool> FireBallProgressUpdated;
	/// <summary>
	/// Param1 is AINotification, Param3 is score
	/// </summary>
	public static Action<bool, NotificationType, int> Notify;
	public static Action BackToMainMenu;

	//Sound
	public static Action VolumeChanged;
	public static Action<AudioSourceHandler> ClipStopped;
	#endregion
}
