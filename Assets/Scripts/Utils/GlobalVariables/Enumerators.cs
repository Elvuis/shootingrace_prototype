﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Input
public enum InputTypes { None, Press, Hold, Release }

//Game
public enum ShotDistances { Near, Middle, Far }

//UI
public enum NotificationType { StandardScore, PerfectScore, BackboardScore, LuckyBall }
public enum RectTweenerStates { Disabled, Playing, ReversePlaying }

//Sound
public enum BusType { Music, SFX }

public static class Enumerators 
{ 

}
