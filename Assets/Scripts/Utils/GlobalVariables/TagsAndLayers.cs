﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TagsAndLayers
{
    #region Tags
    public static string BackboardTag = "Backboard";
    public static string BasketRingTag = "BasketRing";
    public static string GroundTag = "Ground";
    public static string BasketScoreTriggerTag = "BasketScoreTrigger";
    #endregion

    #region Layers
    #endregion
}
