﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShotDataDistanceKvp
{
	[SerializeField] private ShotData_SO shotData;
	[SerializeField] private ShotDistances shotDistance;

	public ShotData_SO ShotData => shotData;
	public ShotDistances ShotDistance => shotDistance;
}
