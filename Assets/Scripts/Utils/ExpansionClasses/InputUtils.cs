﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InputUtils
{
	/// <summary>
	/// Returns the touch position as Viewport.
	/// </summary>
	public static Vector3 PositionInViewport(this Touch touch)
	{
		return Camera.main.ScreenToViewportPoint(touch.position);
	}
}
