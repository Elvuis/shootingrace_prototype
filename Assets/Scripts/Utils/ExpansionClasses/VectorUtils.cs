﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorUtils
{
    public static Vector3 RotateAround(this Vector3 currentPos, Vector3 centerPoint, Vector3 rotationAxis, float angleDegrees)
    {
        Vector3 directionFromCenter = currentPos - centerPoint;
        directionFromCenter.RotateDirection(rotationAxis, angleDegrees);
        return centerPoint + directionFromCenter;
    }

    public static void RotateDirection(this ref Vector3 direction, Vector3 rotationAxis, float angleDegrees)
    {
        Quaternion rotation = Quaternion.AngleAxis(angleDegrees, rotationAxis);
        direction = rotation * direction;
    }
}
