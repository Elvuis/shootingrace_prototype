﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CustomTimer
{
    #region Variables and Properties
    [SerializeField] protected float m_Duration;
    public float Duration => m_Duration;
    [SerializeField] protected bool m_Looping;
    public bool Looping => m_Looping;
    [SerializeField] protected bool m_Unscaled;
    public bool Unscaled { get; set; }

    [SerializeField] protected UnityEvent OnTimerExpiredEvent;

    float lastFrameTimeWasUpdated = -1;
    float expirationFrame = -1;

    public float CurrentTime { get; private set; }
    public float Progress { get { return Mathf.Min(CurrentTime / Duration, 1); } }
    public bool IsActive { get; private set; } = false;
    public bool IsPaused { get; private set; } = false;
    public bool Expired => UnityEngine.Time.frameCount == expirationFrame;
    public float TimeLeft => Duration - CurrentTime;
    #endregion

    public CustomTimer(float duration, bool looping = false, bool unscaled = false, UnityEvent OnTimerEndedEvent = null)
    {
        m_Duration = duration;
        this.m_Looping = looping;
        this.m_Unscaled = unscaled;
        this.OnTimerExpiredEvent = OnTimerEndedEvent;
    }

    #region Methods
    public void SetParameters(float duration, bool looping = false, bool unscaled = false)
    {
        m_Duration = duration;
        this.m_Looping = looping;
        this.m_Unscaled = unscaled;
    }

    public void Start() { IsActive = true; IsPaused = false ; CurrentTime = 0; }
    public void Stop() { CurrentTime = 0; IsActive = false; IsPaused = false; }
    public void Clock()
    {
        if (!IsActive) return;

        if (UnityEngine.Time.frameCount != lastFrameTimeWasUpdated)
        {
            if (m_Unscaled)
                CurrentTime += Mathf.Min(UnityEngine.Time.unscaledDeltaTime, Time.maximumDeltaTime);
            else if (UnityEngine.Time.timeScale != 0)
                CurrentTime += Time.deltaTime;

            lastFrameTimeWasUpdated = UnityEngine.Time.frameCount;
        }

        if (CurrentTime >= Duration) //Check if timer ended
        {
            OnTimerExpiredEvent?.Invoke();

            if (m_Looping)
                CurrentTime = CurrentTime - Duration;
            else
                IsActive = false; //doesn't call Stop() cause progress could still be needed after expiring

            expirationFrame = UnityEngine.Time.frameCount;
        }
    }
    public void Pause(bool paused)
    {
        if (CurrentTime <= 0 || CurrentTime >= Duration)
            return;

        IsActive = !paused;
        IsPaused = paused;
    }

    public void SetProgress(float progress)
    {
        CurrentTime = Mathf.Lerp(0, Duration, progress);
    }

    public void SetCurrentTime(float currentTime)
    {
        this.CurrentTime = currentTime;
    }

    public void AddDuration(float durationToAdd, bool resumeIfEnded = false)
    {
        m_Duration += durationToAdd;
        if (resumeIfEnded)
            IsActive = true;
    }

    public void SetDuration(float newDuration)
    {
        m_Duration = newDuration;
    }

    public void AddUnityEvent(UnityAction unityAction)
    {
        if (OnTimerExpiredEvent == null)
            OnTimerExpiredEvent = new UnityEvent();
        OnTimerExpiredEvent.AddListener(unityAction);
    }

    /// <summary>
    /// This method will clear every non-persistent event.
    /// Non-persistent event: event added via code.
    /// Persistent event: event added via inspector.
    /// </summary>
    public void ClearTimerEndedEvent()
    {
        OnTimerExpiredEvent?.RemoveAllListeners();
    }
    #endregion
}