﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AspectRatioEnforcer : MonoBehaviour
{
    void Start()
    {
#if UNITY_EDITOR
        if(UnityEditor.EditorUserBuildSettings.activeBuildTarget != UnityEditor.BuildTarget.Android)
        {
            Debug.Log("Executing [AspectRatioEnforcer] script.");
            Invoke("SetCorrectAspectRatio", .1f);
        }

#elif !UNITY_ANDROID
        Invoke("SetCorrectAspectRatio", .1f);
#endif
    }

    void SetCorrectAspectRatio()
    {
        //set the desired aspect ratio
        float targetAspect = 9.0f / 16.0f;

        //determine the game window's current aspect ratio
        float windowAspect = (float)Screen.width / (float)Screen.height;

        //current viewport height should be scaled by this amount
        float scaleHeight = windowAspect / targetAspect;

        //obtain camera component so we can modify its viewport
        Camera camera = GetComponent<Camera>();

        Vector2 res = new Vector2();

        //if scaled height is less than current height, add letterbox
        if (scaleHeight < 1.0f)
        {
            Rect rect = camera.rect;

            rect.width = 1.0f;
            rect.height = scaleHeight;
            rect.x = 0;
            rect.y = (1.0f - scaleHeight) / 2.0f;

            res.y = (1.0f - scaleHeight) / 2.0f;

            camera.rect = rect;
        }
        else //add pillarbox
        {
            float scalewidth = 1.0f / scaleHeight;

            Rect rect = camera.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;

            res.x = (1.0f - scalewidth) / 2.0f;

            camera.rect = rect;
        }
    }
}
