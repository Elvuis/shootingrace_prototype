﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PoolObjectsList
{
    public string category;
    public PoolObject_SO[] poolObjects;
}
