﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T singleInstance { get; private set; } = null;

    protected virtual void Awake()
    {
        SetSingleInstance();
    }

    private void OnApplicationQuit()
    {
        singleInstance = null;
    }

    private void SetSingleInstance()
    {
        //There are already an instance
        if (singleInstance != null)
        {
            Destroy(gameObject);
            return;
        }

        T[] instances = GameObject.FindObjectsOfType<T>();

        switch (instances.Length)
        {
            case 1:
            {
                //This is the instance
                singleInstance = instances[0];
                break;
            }
            case 0:
            {
#if UNITY_EDITOR
                Debug.Log("Created a new instance of " + typeof(T).Name);
#endif
                GameObject go = new GameObject();
                go.name = typeof(T).Name;
                go.AddComponent<T>();
                break;
            }
            default:
            {
#if UNITY_EDITOR
                Debug.LogError("Multiple instances of " + typeof(T).Name + " found");
#endif
                //Set this as instance. The others will be destroyed in their Awake
                singleInstance = this as T;
                break;
            }
        }
    }
}
