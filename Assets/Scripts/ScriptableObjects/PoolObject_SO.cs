﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/PoolObject", order = 10)]
public class PoolObject_SO : ScriptableObject
{
    public string poolTag;
    public GameObject objectToPool;
    public int amountToPool;
    public bool shouldExpand;
}