﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/FireBallData", order = 2)]
public class FireBallData_SO : ScriptableObject
{
    [SerializeField] private int fireBallMultiplier;
    [Range(0, 1)]
    [SerializeField] private float perfectShotFireBallGain;
    [Range(0, 1)]
    [SerializeField] private float standardShotFireBallGain;
    [Tooltip("When the fireball is not active, this value will be removed from the FireBall ProgressBar every frame.")]
    [SerializeField] private float fireBallProgressLossValue;
    [SerializeField] private float fireBallBonusDuration;

    public int FireBallMultiplier => fireBallMultiplier;
    public float PerfectShotFireBallGain => perfectShotFireBallGain;
    public float StandardShotFireBallGain => standardShotFireBallGain;
    public float FireBallProgressLossValue => fireBallProgressLossValue;
    public float FireBallBonusDuration => fireBallBonusDuration;
}
