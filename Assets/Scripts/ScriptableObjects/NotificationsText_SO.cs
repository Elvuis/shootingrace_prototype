﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/NotificationsText", order = 3)]
public class NotificationsText_SO : ScriptableObject
{
    [TextArea]
    [SerializeField] private string standardScoreText;
    [TextArea]
    [SerializeField] private string perfectScoreText;
    [TextArea]
    [SerializeField] private string backboardBonusScoreText;
    [TextArea]
    [SerializeField] private string luckyBallText;

    public string GetNotificationText(NotificationType notificationType)
    {
        switch (notificationType)
        {
            case NotificationType.PerfectScore:
                return perfectScoreText;
            case NotificationType.BackboardScore:
                return backboardBonusScoreText;
            case NotificationType.LuckyBall:
                return luckyBallText;
            default:
                return standardScoreText;
        }
    }
}
