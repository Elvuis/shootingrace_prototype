﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/ShotData", order = 0)]
public class ShotData_SO : ScriptableObject
{
    [Tooltip("The distance of the shot from the basket.")]
    [SerializeField] private float baseForwardDistanceFromBasket;
    [Tooltip("The rotations (in degrees) applied for each shot from this distance. They are NOT cumulatives.")]
    [Range(-90, 90)]
    [SerializeField] private float[] rotationDegrees;

    [Space(10)]
    [SerializeField] private float shotForce;
    [SerializeField] private ShotDirectionalInfluence lightShotDirectionalInfluence;
    [SerializeField] private ShotDirectionalInfluence perfectShotDirectionalInfluence;
    [SerializeField] private ShotDirectionalInfluence strongShotDirectionalInfluence;

    [Space(10)]
    [Range(0, 1)]
    [SerializeField] private float perfectShotProgress;
    [Tooltip("Absolute value. More higher this value, more wide the maximum horizontality of the shot.")]
    [SerializeField] protected float maxHorizontalShotAngle;

    public float BaseForwardDistanceFromBasket => baseForwardDistanceFromBasket;
    public int ShotCountFromThisDistance => rotationDegrees.Length;
    public float[] RotationDegrees => rotationDegrees;

    public float ShotForce => shotForce;
    public ShotDirectionalInfluence LightShotDirectionalInfluence => lightShotDirectionalInfluence;
    public ShotDirectionalInfluence PerfectShotDirectionalInfluence => perfectShotDirectionalInfluence;
    public ShotDirectionalInfluence StrongShotDirectionalInfluence => strongShotDirectionalInfluence;

    public float PerfectShotProgress => perfectShotProgress;
    public float MaxHorizontalShotAngle => maxHorizontalShotAngle;
}

[System.Serializable]
public struct ShotDirectionalInfluence
{
    public float shotVerticality;
    [Tooltip("The forward offset will be applied to the direction from the ball to the basket.")]
    public float shotForwardOffset;
}