﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/ScoresData", order = 1)]
public class ScoresData_SO : ScriptableObject
{
    [SerializeField] private int perfectShotScore;
    [SerializeField] private int standardShotScore;
    [Space(10)]
    [SerializeField] private BackboardBonusData[] backboardBonus;
    [Space(10)]
    [SerializeField] private int luckyBallMultiplier;

    public int PerfectShotScore => perfectShotScore;
    public int StandardShotScore => standardShotScore;

    public BackboardBonusData GetBackboardBonusScore() { return backboardBonus[Random.Range(0, backboardBonus.Length)]; }

    public int LuckyBallMultiplier => luckyBallMultiplier;
}

[System.Serializable]
public class BackboardBonusData
{
    public int scoreBonus;
    public Color colorBonus;
}
