﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingRaceHandler : Singleton<ShootingRaceHandler>
{
	#region Variables and Properties
	[Header("Shots and Scores")]
	[SerializeField] protected ShotDataDistanceKvp[] shotsData;
	[Space(10)]
	[SerializeField] protected ScoresData_SO m_scores;
	public static ScoresData_SO Scores => singleInstance.m_scores;
	[SerializeField] protected FireBallData_SO m_fireBallData;
	public static FireBallData_SO fireBallData => singleInstance.m_fireBallData;

	[Space(10)]
	[Range(0, 1)]
	[SerializeField] protected float luckyShotPercentage;

	[Header("Players")]
	//Player
	[SerializeField] private BallShooter player;
	public bool PlayerStartsRotatingClockwise { get; set; }
	public int PlayerScore { get; private set; }
	//AIPlayer
	[SerializeField] private BallShooter AIPlayer;
	public int AIPlayerScore { get; private set; }
	private bool vsAI;

	[Header("Timers")]
	[SerializeField] private float gameDuration;
	[SerializeField] private float drawTime;
	private CustomTimer gameTimer = null;
	public static bool GameTimerActive => singleInstance.gameTimer != null && singleInstance.gameTimer.IsActive;
	public static bool GameTimerPaused => singleInstance.gameTimer != null && singleInstance.gameTimer.IsPaused;

	[SerializeField] private CustomTimer timerToBackboardBonus;

	private AudioSourceHandler gameTimerEndSource = null;
	private bool gameTimerEndedCalled;
	private bool isDraw;
	#endregion

	#region Monobehaviours
	private void OnEnable()
	{
		GlobalEvents.GameStarted += OnGameStarted;
		GlobalEvents.CountDownEnded += OnGameCountDownEnded;
		GlobalEvents.BallScored += OnBallScored;
		GlobalEvents.ShotCompleted += OnShotCompleted;
		GlobalEvents.BackboardBonusScored += OnBackboardBonusScored;
		GlobalEvents.BackToMainMenu += OnGameEnded;
	}
	private void OnDisable()
	{
		GlobalEvents.GameStarted -= OnGameStarted;
		GlobalEvents.CountDownEnded -= OnGameCountDownEnded;
		GlobalEvents.BallScored -= OnBallScored;
		GlobalEvents.ShotCompleted -= OnShotCompleted;
		GlobalEvents.BackboardBonusScored -= OnBackboardBonusScored;
		GlobalEvents.BackToMainMenu -= OnGameEnded;
	}
	private void Update()
	{
		if (GameTimerActive)
		{
			gameTimer.Clock();
			GlobalEvents.GameTimerUpadated?.Invoke(gameTimer.Progress);
		}

		if (timerToBackboardBonus != null && timerToBackboardBonus.IsActive)
			timerToBackboardBonus.Clock();
	}
	#endregion

	#region Methods
	private void OnGameStarted(bool vsAI)
	{
		this.vsAI = vsAI;
		SetupPlayers();
		gameTimerEndedCalled = false;
		isDraw = false;
	}

	private void OnGameCountDownEnded()
	{
		if (!isDraw) //OnGameStart
		{
			SetupGameTimer();
			StartBackboardBonusTimer();
		}
		else //Draw CountDown
		{
			gameTimer.SetDuration(drawTime);
			gameTimer.Start();
			timerToBackboardBonus.Pause(false);
		}
	}

	private void SetupPlayers()
	{
		PlayerStartsRotatingClockwise = Random.Range(0, 100) < 50;
		player.gameObject.SetActive(true);
		player.Init();
		PlayerScore = 0;

		if (!vsAI)
			return;

		AIPlayer.gameObject.SetActive(true);
		AIPlayer.Init();
		AIPlayerScore = 0;
	}

	private void SetupGameTimer()
	{
		gameTimer = new CustomTimer(gameDuration);
		gameTimer.ClearTimerEndedEvent();
		gameTimer.AddUnityEvent(OnGameTimeExpired);
		gameTimer.Start();
	}

	private void StartBackboardBonusTimer()
	{
		timerToBackboardBonus.ClearTimerEndedEvent();
		timerToBackboardBonus.AddUnityEvent(() => { GlobalEvents.EnableBackboardBonus?.Invoke(); });
		timerToBackboardBonus.Start();
	}

	private void OnGameTimeExpired()
	{
		if (!timerToBackboardBonus.IsPaused)
		{
			timerToBackboardBonus.Pause(true);
		}

		if (!gameTimerEndedCalled)
		{
			//Sound
			gameTimerEndSource = AudioManager.singleInstance.Play(PoolTags.GameTimerEnded);
			gameTimerEndSource.OnClipEnded.RemoveAllListeners();
			gameTimerEndSource.OnClipEnded.AddListener(() => gameTimerEndSource = null);
			gameTimerEndedCalled = true;
		}

		//waiting for every player to stop shot. If the timer is expired, this method will
		//be called from OnShotCompleted() after the target player shot is not in progress anymore
		if (player.ShotInProgress || AIPlayer.ShotInProgress)
			return;
		else
		{
			//wait the end of the gameTimerEnd sound
			if (gameTimerEndSource == null) 
			{
				PlayersShotsStopped();
			}
			else
			{
				Invoke("PlayersShotsStopped", 1.5f);
			}
		}

	}

	private void PlayersShotsStopped()
	{
		if (vsAI)
		{
			if (AIPlayerScore == PlayerScore)
			{
				isDraw = true;
				gameTimerEndedCalled = false;
				GlobalEvents.GameDraw?.Invoke();
				return;
			}
			else
			{
				GlobalEvents.VsAIModeGameEnded?.Invoke(PlayerScore > AIPlayerScore, PlayerScore, AIPlayerScore);
			}
		}
		else
		{
			GlobalEvents.SoloModeGameEnded?.Invoke(PlayerScore);
		}

		OnGameEnded();
	}

	private void OnGameEnded()
	{
		gameTimer.Stop();
		timerToBackboardBonus.Stop();

		player.gameObject.SetActive(false);
		AIPlayer.gameObject.SetActive(false);

		PoolManager.singleInstance.PushObjectsList(PoolTags.Ball, PoolManager.singleInstance.GetActivePoolObjects(PoolTags.Ball));

		AudioManager.singleInstance.StopAllSoundEffects();
	}

	private void OnBallScored(bool isAIBall, int score, float fireBallProgressGained)
	{
		if (!isAIBall)
		{
			PlayerScore += score;
			if (!player.isOnFire)
				player.AddFireBallProgress(fireBallProgressGained);
		}
		else
		{
			AIPlayerScore += score;
			if (!AIPlayer.isOnFire)
				AIPlayer.AddFireBallProgress(fireBallProgressGained);
		}
	}

	private void OnShotCompleted(bool isAIBall, bool scoredThisShot)
	{
		BallShooter targetPlayer = !isAIBall ? player : AIPlayer;

		if (!scoredThisShot)
			targetPlayer.EmptyFireBallProgress();

		bool nextShotIsLuckyShot = Random.Range(0, 100) < (luckyShotPercentage * 100) && GameTimerActive;
		targetPlayer.InitShot(scoredThisShot, nextShotIsLuckyShot);

		if (targetPlayer == player && nextShotIsLuckyShot) //For lucky ball notification, score is not needed.
			GlobalEvents.Notify?.Invoke(false, NotificationType.LuckyBall, 0);

		//If time ends, shots that was already performed can still score points.
		if (!GameTimerActive)
			OnGameTimeExpired();
	}

	private void OnBackboardBonusScored()
	{
		StartBackboardBonusTimer();
	}

	public ShotDataDistanceKvp GetShotDataDistanceKvp(ShotDistances targetDistance)
	{
		for (int i = 0; i < shotsData.Length; i++)
		{
			if (shotsData[i].ShotDistance == targetDistance)
				return shotsData[i];
		}
#if UNITY_EDITOR
		Debug.LogError("ShotData with distance " + targetDistance + " doesn't exists.");
#endif
		return null;
	}
	#endregion
}
