﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioSourceHandler : MonoBehaviour
{
	#region Variables and Properties
	private AudioSource m_AudioSource;
	private AudioSource AudioSource { get{ if (!m_AudioSource) m_AudioSource = GetComponent<AudioSource>(); return m_AudioSource; } }

	[SerializeField] private BusType m_busType;
	public BusType BusType => m_busType;
	[Range(0, 1)]
	[SerializeField] private float volume;
	public UnityEvent OnClipEnded = new UnityEvent();
	[Space(10)]
	[SerializeField] private string poolTag;

	private bool played;
	private bool isPaused;
	#endregion

	#region Monobehaviours
	private void OnEnable()
	{
		GlobalEvents.VolumeChanged += SetVolume;
	}
	private void OnDisable()
	{
		GlobalEvents.VolumeChanged -= SetVolume;
	}

	private void Update()
	{
		if (played && !AudioSource.isPlaying && !isPaused)
		{
			OnClipEnded?.Invoke();
			Stop();
		}
	}
	#endregion

	#region Methods
	public void Play() 
	{
		SetVolume();
		AudioSource.Play();
		played = true;
		isPaused = false;
	}

	public void SetVolume()
	{
		AudioSource.volume = volume * AudioManager.GetBusVolume(BusType);
	}

	public void Stop()
	{
		AudioSource.Stop();
		played = false;
		GlobalEvents.ClipStopped?.Invoke(this);
		BackOnPool();
	}

	public void Pause(bool pause)
	{
		if (pause)
			AudioSource.Pause();
		else
			AudioSource.UnPause();

		isPaused = pause;
	}

	public void BackOnPool()
	{
		PoolManager.singleInstance.Push(poolTag, this.gameObject);
	}
	#endregion
}
