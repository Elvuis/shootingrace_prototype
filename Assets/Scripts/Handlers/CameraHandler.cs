﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHandler : Singleton<CameraHandler>
{
	#region Variables and Properties
	Transform m_Transform;
	Transform Transform { get { if (!m_Transform) m_Transform = transform; return m_Transform; } }

	[Header("Target")]
	[SerializeField] private Transform target;

	[Header("Before the shot")]
	[SerializeField] private float BackwardOffsetFromShotPosition;
	[SerializeField] private float verticalOffsetFromShotPosition;

	[Header("During the shot")]
	[SerializeField] private float minDistanceFromTarget;
	[SerializeField] private float verticalOffsetFromTarget;
	[Tooltip("Higher the value, lower the speed of the camera.")]
	[Range(0, 1)]
	[SerializeField] private float cameraSmoothFactor;
	private Vector3 smoothDampVelocity;
	private Vector3 cameraTargetPosition;

	bool playerShotPerformed = false;
	#endregion

	#region Monobehaviours
	private void FixedUpdate()
	{
		ProcessCameraMovement();
	}
	#endregion

	#region Methods
	public void OnInitShot(Vector3 shotPosition)
	{
		playerShotPerformed = false;

		Vector3 forwardDirection = Basket.singleInstance.GetDirectionFromTargetPosition(shotPosition);
		forwardDirection.y = 0;
		Transform.position = shotPosition - forwardDirection * BackwardOffsetFromShotPosition;
		Transform.position = new Vector3(Transform.position.x, Transform.position.y + verticalOffsetFromShotPosition, Transform.position.z);

		Transform.LookAt(target);

		SetCameraTargetPosition();
	}

	public void OnPlayerShoot()
	{
		playerShotPerformed = true;
	}

	private void SetCameraTargetPosition()
	{
		cameraTargetPosition = target.transform.position;
		cameraTargetPosition -= (target.transform.position - Transform.position).normalized * minDistanceFromTarget;
		cameraTargetPosition.y = target.transform.position.y + verticalOffsetFromTarget;
	}

	private void ProcessCameraMovement()
	{
		if (!playerShotPerformed)
			return;

		Transform.position = Vector3.SmoothDamp(Transform.position, cameraTargetPosition, ref smoothDampVelocity, cameraSmoothFactor);
		Transform.LookAt(target);
	}
	#endregion
}
